from django.shortcuts import render, redirect
from .models import Schedule
from . import forms


# Create your views here.
def pertama(request):
    return render(request,'PPW.html')

def about(request):
    return render(request,'About.html')

def experience(request):
    return render(request,'Experience.html')

def kontak(request):
    return render(request,'Contact.html')

def bonus(request):
    return render(request,'Bonus.html')

def schedule(request):
    schedules = Schedule.objects.all().order_by('date')
    return render(request, 'schedule.html', {'schedules': schedules})

def schedule_create(request):
    if request.method == 'POST':
        form = forms.ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            form = forms.ScheduleForm()


    else:
        form = forms.ScheduleForm()
    return render(request, 'schedule_create.html', {'form': form})


def schedule_delete(request):
	Schedule.objects.all().delete()
	return render(request, "schedule.html")



def schedule_hilang(request, id):
    schedule = Schedule.objects.get(id=id)
    schedule.delete()
    schedules = Schedule.objects.all()
    return render(request, 'schedule.html', {'schedules': schedules})
