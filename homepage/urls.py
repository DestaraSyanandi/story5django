from django.urls import path
from . import views


app_name = 'homepage'

urlpatterns = [
    path('', views.pertama, name='pertama'),
    path('about/', views.about, name='about'),
    path('experience/', views.experience, name='experience'),
    path('contact/', views.kontak, name='kontak'),
    path('bonus/', views.bonus, name='bonus'),
    path('schedule/', views.schedule, name='schedule'),
    path('schedule/create', views.schedule_create, name='schedule_create'),
    path('schedule/delete', views.schedule_delete, name='schedule_delete'),
    path('schedule/hilang/<int:id>', views.schedule_hilang, name='schedule_hilang'),



]
